# NEW TEXTMATE
has 2 branches. "vue" branch for the web app and "api" for the server request and response.

```bash
    git clone https://gervic23@bitbucket.org/gervic23/new_textmate.git
```

AND

```bash
    git fetch && git checkout vue
```

AND
```bash
    git fetch && git checkout api
```